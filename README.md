# LemonadeApp

A simple, interactive app that lets you juice lemons when you tap the image on screen until you have a glass of lemonade. The application has multiple solutions which are stored seperately on each branch which are listed below

- first-working-solution -> This branch contains the first attempt to create app without hints, does not contain squeeze logic and is missing on some UI enhancements. But it still completes the lemonade journey from step 1 to step 4 and repeating the process.
