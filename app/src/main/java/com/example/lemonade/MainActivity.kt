package com.example.lemonade

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.lemonade.ui.theme.LemonadeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LemonadeTheme {
                // A surface container using the 'background' color from the theme
//                Surface(
//                    modifier = Modifier.fillMaxSize(),
//                    color = MaterialTheme.colorScheme.background
//                ) {
//                    Greeting("Android")
//                }
                LemonadeApp()
            }
        }
    }
}

@Composable
fun LemonadeAppHeader(modifier: Modifier = Modifier) {
    Surface(color = Color.Yellow) {
        Box(modifier = Modifier
            .fillMaxWidth(1f)
            .height(100.dp)
        ) {
            Text(text = "Lemonade",
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .align(alignment = Alignment.CenterEnd),
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold
                )
        }
    }
}

@Composable
fun LemonadeJourney(modifier: Modifier = Modifier) {
    var img_counter by remember { mutableStateOf(1) }
    val imgResource = when (img_counter) {
        1 -> R.drawable.lemon_tree
        2 -> R.drawable.lemon_squeeze
        3 -> R.drawable.lemon_drink
        4 -> R.drawable.lemon_restart
        else -> R.drawable.lemon_tree
    }
    val imgDescription = when (img_counter) {
        1 -> R.string.img1_description
        2 -> R.string.img2_description
        3 -> R.string.img3_description
        4 -> R.string.img4_description
        else -> R.string.img1_description
    }
    val imgContentDescription = when (img_counter) {
        1 -> R.string.lemon_tree
        2 -> R.string.lemon
        3 -> R.string.glass_of_lemonade
        4 -> R.string.empty_glass
        else -> R.string.lemon_tree
    }
    //var squeezeLemon = (2..4).random()
    Column(modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally) {
        TextButton(onClick = {
            if (img_counter < 4) {
//                if(img_counter == 2) {
//                    while (squeezeLemon > 0) {
//                        squeezeLemon--
//                        continue
//                    }
//                }
                img_counter += 1
            }
            else {
                img_counter = 1
            }
        }, modifier = Modifier.background(color = Color(195, 236, 210))) {
            Image(painter = painterResource(id = imgResource), contentDescription = stringResource(
                id = imgContentDescription
            ), contentScale = ContentScale.Fit)
        }
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = stringResource(id = imgDescription))
    }
}

@Preview(showBackground = true)
@Composable
fun LemonadeApp() {
    Column {
        LemonadeAppHeader(modifier = Modifier)
        LemonadeJourney(modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center))
    }

//    LemonadeAppHeader(modifier = Modifier.fillMaxWidth())
}

//@Composable
//fun Greeting(name: String, modifier: Modifier = Modifier) {
//    Text(
//        text = "Hello $name!",
//        modifier = modifier
//    )
//}
//
//@Preview(showBackground = true)
//@Composable
//fun GreetingPreview() {
//    LemonadeTheme {
//        Greeting("Android")
//    }
//}